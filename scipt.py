import pandas as pd 
import matplotlib.pyplot as plt 
from numpy import arange

# ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),ncol=3, fancybox=True)
def change(df):
    df['Change'] = df.apply(lambda row:
    (
        row['Adj Close']-df.iloc[0,4])/df.iloc[0,4]*100,axis=1
    )
    return df

def cspine(ax):
    ax.spines['left'].set_position(('axes', 0))
    ax.spines['bottom'].set_position(('data',0))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)


dow = change(pd.read_csv('datasets/DJI.csv', header=0, index_col=0, parse_dates=True))
sp = change(pd.read_csv('datasets/GSPC.csv', header=0, index_col=0, parse_dates=True))
nsdq = change(pd.read_csv('datasets/IXIC.csv', header=0, index_col=0, parse_dates=True))
vix = change(pd.read_csv('datasets/VIX.csv', header=0, index_col=0, parse_dates=True))
zm = change(pd.read_csv('datasets/ZM.csv', header=0, index_col=0, parse_dates=True))
nflx = change(pd.read_csv('datasets/NFLX.csv', header=0, index_col=0, parse_dates=True))
gold = change(pd.read_csv('datasets/gold.csv', header=0, index_col=0, parse_dates=True))
oil = change(pd.read_csv('datasets/oil.csv', header=0, index_col=0, parse_dates=True))

fig, ax = plt.subplots()
ax.plot(dow.index, dow['Change'], color='#ef476f', label='^DJI')
ax.plot(sp.index, sp['Change'], color='#06d6a0', label='^GSPC')
ax.plot(nsdq.index, nsdq['Change'], color='#ffd166', label='^IXIC')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),ncol=3, fancybox=True)
ax.set_xlabel('Date')
ax.set_ylabel('Evolution of Index (%)')
plt.yticks(arange(-40,11,10))
cspine(ax)
plt.savefig('outputs/vis1.png', dpi=1200)

fig, ax = plt.subplots()
ax.plot(nflx.index, nflx['Change'], color='#ef476f', label='NFLX')
ax.plot(zm.index, zm['Change'], color='#06d6a0', label='ZM')
# ax.plot(nsdq.index, nsdq['Change'], color='#ffd166', label='^IXIC')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),ncol=3, fancybox=True)
ax.set_xlabel('Date')
ax.set_ylabel('Evolution of stock price (%)')
# plt.yticks(arange(-40,11,10))
cspine(ax)
plt.savefig('outputs/vis2.png', dpi=1200)

fig, ax = plt.subplots()
ax1 = ax.twinx()
ax.plot(sp.index, sp['Adj Close'], color='#ef476f', label='^GSPC')
ax1.plot(vix.index, vix['Adj Close'], color='#06d6a0', label='^VIX')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),ncol=3, fancybox=True)
ax1.legend(loc='upper center', bbox_to_anchor=(0.7, 1.1),ncol=3, fancybox=True)
ax.set_xlabel('Date')
ax.set_ylabel('Adjusted close of ^GSPC')
ax1.set_ylabel('Adjusted close of ^VIX')
# ax.spines['top'].set_visible(False); ax1.spines['top'].set_visible(False)
plt.savefig('outputs/vis3.png', dpi=1200)

fig, ax = plt.subplots()
ax1 = ax.twinx()
ax.plot(oil.index, oil['Adj Close'], color='#ef476f', label='Crude Oil')
ax1.plot(gold.index, gold['Adj Close'], color='#06d6a0', label='Gold')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),ncol=3, fancybox=True)
ax1.legend(loc='upper center', bbox_to_anchor=(0.7, 1.1),ncol=3, fancybox=True)
ax.set_xlabel('Date')
ax.set_ylabel('Adjusted close of Crude Oil')
ax1.set_ylabel('Adjusted close of Gold')
# ax.spines['top'].set_visible(False); ax1.spines['top'].set_visible(False)
plt.savefig('outputs/vis4.png', dpi=1200)